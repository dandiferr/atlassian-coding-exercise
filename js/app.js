var app = angular.module("MyApp", ['ngRoute'])
.config(function($routeProvider, $locationProvider) {

    $routeProvider
    .when('/spaceexplorer/:id', {
        templateUrl: 'partials/home.html',
        controller: 'HomeController'
    })
    .otherwise({
      redirectTo : "/spaceexplorer/yadj1kx9rmg0"
  });
});

app.controller("HomeController", ['$scope', 'SpaceExplorerAPI', 'UserExplorerAPI', '$routeParams',
    function ($scope, SpaceExplorerAPI, UserExplorerAPI, $routeParams) {
        var spaceId = $routeParams.id;

        $scope.tab = {
            switch: true
        };

        $scope.getUser = function(userId) {
            return UserExplorerAPI.getUser(userId).fields.name;
        }

        $scope.toggleTab = function(tabValue) {
            switch (tabValue) {
                case "entries":
                $scope.predicate = "";
                $scope.reverse = false;
                $scope.tab.switch = true;
                getEntryData();
                break;
                case "assets":
                $scope.predicate = "";
                $scope.reverse = false;
                $scope.tab.switch = false;
                getAssetData();
                break;
            }
        }

        function getEntryData() {
            $scope.entryRows = SpaceExplorerAPI.getEntriesForSpaceId(spaceId).items;
        }

        function getAssetData() {
            $scope.entryRows = SpaceExplorerAPI.getAssetsForSpaceId(spaceId).items;
        }

        function getSpaceData() {
            var data = SpaceExplorerAPI.getSpaceById(spaceId);
            if (isEmpty(data)) return;
            $scope.header = data.fields.title;
            $scope.subheader = "created by " + $scope.getUser(data.sys.createdBy);
            $scope.textBody = data.fields.description;
        }

        function init() {
            getEntryData();
            getSpaceData();
        }

        init();

        function isEmpty(obj) {
            for(var key in obj) {
                if(obj.hasOwnProperty(key))
                    return false;
            }
            return true;
        }
    }
    ]);

app.controller("MainController", ['$scope', 'SpaceExplorerAPI',
    function ($scope, SpaceExplorerAPI) {

        function getData() {
            $scope.spaceRows = SpaceExplorerAPI.getSpace().items;
        }

        getData();
    }
    ]);