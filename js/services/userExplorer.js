angular.module('MyApp')
  .factory('UserExplorerAPI', function($http) {
    return {
      getUsers: function() {
        // return $http.get('/users');
        return getUsers();
      },
      getUser: function(userId) {
        // return $http.get('/users/' + userId);
        return getUser(userId);
      }
    };

    function getUsers() {
      return {
        "sys": {
          "type": "Array"
        },
        "total": 1,
        "skip": 0,
        "limit": 100,
        "items": [{
              "fields": {
                "name": "Alana Atlassian",
                "role": "Author"
              },
              "sys": {
                "id": "4FLrUHftHW3v2BLi9fzfjU",
                "type": "User"
              }
            },{
              "fields": {
                "name": "John Doe",
                "role": "Editor"
              },
              "sys": {
                "id": "4FLrUHftHW3v2BLi9fzfjU2",
                "type": "User"
              }
            }]
      }
    }

    function getUser(userId) {
      if (userId == "4FLrUHftHW3v2BLi9fzfjU") {
        return {
          "fields": {
            "name": "Alana Atlassian",
            "role": "Author"
          },
          "sys": {
            "id": "4FLrUHftHW3v2BLi9fzfjU",
            "type": "User"
          }
        }
      }

      if (userId == "4FLrUHftHW3v2BLi9fzfjU2") {
        return {
          "fields": {
            "name": "John Doe",
            "role": "Editor"
          },
          "sys": {
            "id": "4FLrUHftHW3v2BLi9fzfjU2",
            "type": "User"
          }
        }
      }
      return {"message": "User not found"};
    }
  });